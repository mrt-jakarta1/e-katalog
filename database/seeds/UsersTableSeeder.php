<?php

use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        User::create([
            'full_name' =>  'Market Place User',
            'email'     =>  'user@mail.com',
            'password'  =>  bcrypt('111111'),
        ]);
    }
}