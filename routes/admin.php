<?php

Route::namespace('Admin')->prefix('admin')->group(function () {

    Route::get('login', 'LoginController@showLoginForm')->name('admin.login');
    Route::post('login', 'LoginController@login')->name('admin.login.post');
    Route::get('logout', 'LoginController@logout')->name('admin.logout');
    
    Route::middleware(['auth:admin'])->group(function () {
        Route::get('/', function () {
            return view('admin.dashboard.index');
        })->name('admin.dashboard');
        Route::get('/data_admin', function () {
            return view('admin.dashboard.dataadmin');
        })->name('admin.dataadmin');
    });
    
});