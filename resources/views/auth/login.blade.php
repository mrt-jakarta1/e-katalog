@extends('site.app')
@section('title', 'Msauk')
@section('content')
<center>
<div class="container mt-5">
    <div class="row">
        <div class="col-12">
            <div class="form-daftar p-4 mb-5">
                <h4><b>Masuk Sekarang</b></h4>
                <form class="needs-validation mt-4" novalidate action="{{ route('login') }}" method="POST" role="form">
                @csrf
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Alamat Email" name="email" value="{{ old('email') }}" required>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Kata Sandi" name="password" required>
                    </div>
                    <button type="submit" class="btn btn-block btn-md btn-primary mt-4">Masuk</button>
                </form>
                <div class="text-center font-14 mt-3">
                    <a href="{{ route('register') }}">Tidak punya akun ? Daftar sekarang</a>
                </div>
            </div>
        </div>
    </div>
</div>
</center>
@endsection