@extends('site.app')
@section('title', 'Daftar')
@section('content')
<center>
<div class="container mt-5">
    <div class="row">
        <div class="col-12">
            <div class="form-daftar p-4 mb-4">
                <h4><b>Daftar Sekarang</b></h4>
                <form class="needs-validation mt-4" novalidate action="{{ route('register') }}" method="POST" role="form">
                @csrf
                    <div class="form-group">
                        <input type="text" class="form-control font-14" placeholder="Nama Lengkap" name="full_name" value="{{ old('full_name') }}" required>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control font-14" placeholder="Alamat Email" name="email" value="{{ old('email') }}" required>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control font-14" placeholder="Kata Sandi" name="password" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control font-14" placeholder="Dept" name="dept" value="{{ old('dept') }}">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control font-14" placeholder="Devisi" name="devisi" value="{{ old('devisi') }}">
                    </div>
                    <button type="submit" class="btn btn-block btn-md btn-primary mt-4 font-14">Daftar</button>
                </form>
                <div class="text-center font-14 mt-3 mb-3 font-12">
                    <a>Dengan mendaftar, saya menyetujui</a><br><a href="#"><b>Syarat dan Ketentuan</b></a> serta <a href="#"><b>Kebijakan Privasi</b></a>
                </div>
                <div class="text-center font-14 mt-4 font-12">
                    <a href="{{ route('login') }}">Sudah punya akun ? Masuk sekarang</a>
                </div>
            </div>
        </div>
    </div>
</div>
</center>
@endsection