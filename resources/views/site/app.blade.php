<!DOCTYPE HTML>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link rel="shortcut icon" href="{{ asset('/site/img/logo-mrt.png') }}" type="image/png">

        <title>@yield('title') | {{ config('app.name') }}</title>
        
        @include('site.partials.styles')
    </head>

    <body class="bg-light">
        @include('site.partials.header')

        @yield('content')

        @include('site.partials.footer')

        @include('site.partials.scripts')
    </body>

</html>