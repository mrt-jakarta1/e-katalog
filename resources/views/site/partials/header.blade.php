<div class="modal fade modalMasuk" id="ModalMasuk" style="z-index: 1000000;" tabindex="-1" role="dialog" aria-labelledby="ModalLabelMasuk" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content modal-content-masuk">
            <div class="modal-header">
                <b class="modal-title" id="ModalLabelMasuk">Masuk ke MRT e-Marketplace</b>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('login') }}" method="POST" role="form" class="needs-validation" novalidate>
                @csrf
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Alamat Email" name="email" value="{{ old('email') }}" required>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Kata Sandi" name="password" required>
                    </div>
                    <div class="text-right font-12">
                        <a href="#">Lupa Kata Sandi ?</a>
                    </div>
                    <button type="submit" class="btn btn-block btn-md btn-primary mt-3">Masuk</button>
                </form>
                <div class="text-center font-14 mt-3 mb-3">
                    <a href="{{ route('register') }}">Tidak punya akun ? Daftar sekarang</a>
                </div>
            </div>
        </div>
    </div>
</div>

<nav class="navbar navbar-expand-lg navbar-white bg-white shadow-sm sticky-top">
    <div class="container">
    
        <a class="navbar-brand font-16 logos" href="{{ route('beranda') }}">
            <img src="{{ URL::asset('/site/img/logo-mrt.png') }}" width="50px" height="50px" alt="Logo">
            <b>e-Marketplace</b>
        </a>

        <button class="navbar-toggler toggler-button" type="button" data-toggle="collapse" data-target="#navbarTarget" aria-controls="navbarTarget" aria-expanded="false" aria-label="Toggle navigation">
            <div class="animated-icon"><span></span><span></span><span></span><span></span></div>
        </button>

        <div class="collapse navbar-collapse" id="navbarTarget">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('beranda') }}">Beranda</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Kategori</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Keyboard</a>
                        <a class="dropdown-item" href="#">Laptop</a>
                        <a class="dropdown-item" href="#">Mouse</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Tentang Kami</a>
                </li>
            </ul>

            @guest
                <div class="navbar-nav">
                    <button type="button" class="btn btn-primary btn-lg radius-20 font-14" data-toggle="modal" data-target="#ModalMasuk">
                        Masuk / Daftar
                    </button>
                </div>
            @else
                <div class="navbar-nav">
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#">
                            <i class="fa fa-shopping-cart mobile-sl-icon"></i>
                            <span class="mobile-sl-text font-16">Keranjang Belanja</span>
                        </a>
                    </li>
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#">
                            <i class="fa fa-bell-o mobile-sl-icon"></i>
                            <span class="mobile-sl-text font-16">Notifikasi</span>
                        </a>
                    </li>
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#">
                            <i class="fa fa-envelope-o mobile-sl-icon"></i>
                            <span class="mobile-sl-text font-16">Kotak Pesan</span>
                        </a>
                    </li>
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-user-o mobile-sl-icon"></i>
                            <span class="mobile-sl-text font-16">Profil Saya</span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                                <i class="fa fa-user-o fa-sm fa-fw mr-2 text-gray-400"></i>
                                Profil
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out fa-sm fa-fw mr-2 text-gray-400"></i>
                                {{ __('Keluar') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </div>
            @endguest

        </div>
    </div>
</nav>