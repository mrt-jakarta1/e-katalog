<footer class="bg-white sticky-footer page-footer font-small shadow-sm mt-4">

  <div class="container pt-4">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 text-center">
          <a href="#">
            <img src="{{ URL::asset('/site/img/logo-mrt.png') }}" width="100px" height="100px" alt="Logo">
          </a>
        </div>
        <div class="col-lg-4 text-center">
          <p class="mt-3">Wisma Nusantara Lt. 21 <br> Jln. MH. Thamrin 59 <br> Jakarta 10350 – Indonesia</p>
        </div>
        <div class="col-lg-4 text-center">
          <p class="mt-3">Telp. (+62-21) 390 6454 <br> Faks. (+62-21) 315 5846/ <br> (+62-21) 314 2273</p>
        </div>
      </div>
    </div>
  </div>

  <hr>

  <div class="footer-copyright text-center pb-3 font-12">

    © 2019 Copyright <a href="#">MRT e-Marketplace</a>

  </div>

</footer>