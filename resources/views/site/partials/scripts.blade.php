<script type="text/javascript" src="{{ asset('/site/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/site/js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/site/js/bootstrap.bundle.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/site/js/bootstrap.min.js') }}"></script>

    <script>
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                var forms = document.getElementsByClassName('needs-validation');
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                    form.classList.add('was-validated');
                }, false);
            });
            }, false);
        })();
    </script>

<script>
    $(document).ready(function () {
        $('.toggler-button').on('click', function () {
            $('.animated-icon').toggleClass('open');
        });
    });
</script>