<link rel="stylesheet" type="text/css" href="{{ asset('/site/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('/site/css/animated.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/site/css/custom-mrt.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/site/css/responsive.css') }}">