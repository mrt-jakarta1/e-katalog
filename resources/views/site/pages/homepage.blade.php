@extends('site.app')
@section('title', 'Beranda')

@section('content')

<div class="container dekstop-carousel mt-4">
    <div id="dekstop-carousel-controls" class="carousel slide d-flex justify-content-center" data-ride="carousel">
        <div class="carousel-inner radius-15">
            <div class="carousel-item active">
                <img class="d-block dekstop-carousel-item" src="{{ URL::asset('/site/img/1.png') }}" alt="Slide #1">
            </div>
            <div class="carousel-item">
                <img class="d-block dekstop-carousel-item" src="{{ URL::asset('/site/img/2.png') }}" alt="Slide #2">
            </div>
        </div>

        <div class="input-group md-form form-sm form-2 pl-0 dekstop-carousel-search">
            <input class="form-control my-0 py-1 font-12" type="text" placeholder="Cari Produk Disini ..." aria-label="Search">
            <div class="input-group-append">
                <button class="input-group-text btn-search font-12"><i class="fa fa-search" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>

    <a href="#dekstop-carousel-controls" class="carousel-control-prev" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
	</a>
	
    <a href="#dekstop-carousel-controls" class="carousel-control-next" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<div class="mobile-carousel mt-4">
    <div id="mobile-carousel-controls" class="carousel slide d-flex justify-content-center" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block mobile-carousel-item" src="{{ URL::asset('/site/img/1.png') }}" alt="Slide #1">
            </div>
            <div class="carousel-item">
                <img class="d-block mobile-carousel-item" src="{{ URL::asset('/site/img/2.png') }}" alt="Slide #2">
            </div>
        </div>

        <div class="input-group md-form form-sm form-2 pl-0 mobile-carousel-search">
            <input class="form-control my-0 py-1 radius-10 font-12" type="text" placeholder="Cari Produk Disini ..." aria-label="Search">
            <div class="input-group-append">
                <button class="input-group-text btn-search radius-10 font-12"><i class="fa fa-search" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>

    <a href="#mobile-carousel-controls" class="carousel-control-prev" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
	</a>
	
    <a href="#mobile-carousel-controls" class="carousel-control-next" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<div class="container content mt-5">
    <div class="container dekstop-content">
        <div class="row m-4">
        
            <?php

                for ($i = 0; $i < 3; $i++){

                    echo "<div class='col-3'>
                    <a href='#'>
                    <div class='card product-card'>
                        <img class='card-img-top' src='/site/img/keyboard.jpg' alt='Razer RGB Keyboard'>
                        <div class='card-body'>
                            <h6 class='nowrap-text font-16'>Razer RGB Keyboard</h6>
                            <h6 class='nowrap-text font-14'>Stok 24</h6>
                        </div>
                    </div>
                    </a>
                    </div>";
                }

            ?>

            <?php

                for ($i = 0; $i < 3; $i++){

                    echo "<div class='col-3'>
                    <a href='#'>
                    <div class='card product-card'>
                        <img class='card-img-top' src='/site/img/laptop.png' alt='Asus Republic Of Gaming'>
                        <div class='card-body'>
                            <h6 class='nowrap-text font-16'>Asus Republic Of Gaming</h6>
                            <h6 class='nowrap-text font-14'>Stok 5</h6>
                        </div>
                    </div>
                    </a>
                    </div>";
                }

            ?>
            
            <div class="container">
                <a href="#" class="btn btn-block btn-primary">Lihat Semua</a>
            </div>

        </div>

    </div>

    <div class="mobile-content">
        <div class="row">
        
            <?php

                for ($i = 0; $i < 3; $i++){

                    echo "<div class='col-6'>
                    <a href='#'>
                    <div class='card product-card'>
                        <img class='card-img-top' src='/site/img/keyboard.jpg' alt='Razer RGB Keyboard'>
                        <div class='card-body'>
                            <h6 class='nowrap-text font-12'>Razer RGB Keyboard</h6>
                            <h6 class='nowrap-text font-12'>Stok 24</h6>
                        </div>
                    </div>
                    </a>
                    </div>";
                }

            ?>

            <?php

                for ($i = 0; $i < 3; $i++){

                    echo "<div class='col-6'>
                    <a href='#'>
                    <div class='card product-card'>
                        <img class='card-img-top' src='/site/img/laptop.png' alt='Asus Republic Of Gaming'>
                        <div class='card-body'>
                            <h6 class='nowrap-text font-12'>Asus Republic Of Gaming</h6>
                            <h6 class='nowrap-text font-12'>Stok 5</h6>
                        </div>
                    </div>
                    </a>
                    </div>";
                }

            ?>

            <div class="container">
                <a href="#" class="btn btn-block btn-primary">Lihat Semua</a>
            </div>

        </div>

    </div>

</div>

@stop