<ul class="navbar-nav bg-gradient-white sidebar sidebar-dark accordion" id="accordionSidebar">

    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/admin">
        <div class="sidebar-brand-icon">
            <img src="{{ URL::asset('/images/logo-mrt.png') }}" width="50px" height="50px" alt="Logo">
        </div>
        <div class="sidebar-brand-text mx-3">Marketplace</div>
    </a>

    <hr class="sidebar-divider my-0">

    <li class="nav-item @if ($pageSlug == 'dashboard') active @endif">
        <a class="nav-link" href="{{ route('admin.dashboard') }}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
    </li>

    <hr class="sidebar-divider">

    <div class="sidebar-heading">
        Antarmuka
    </div>

    <li class="nav-item">
        <a class="nav-link @if ($pageSlug != 'data_admin' or $pageSlug != 'data_pengguna') collapsed @endif" href="#" data-toggle="collapse" data-target="#collapseNol" aria-expanded="true" aria-controls="collapseNol">
            <i class="fas fa-fw fa-th-list"></i>
            <span>Manajemen Pengguna</span>
        </a>
        <div id="collapseNol" class="collapse @if ($pageSlug == 'data_admin' or $pageSlug == 'data_pengguna') show @endif" aria-labelledby="headingNol" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item @if ($pageSlug == 'data_admin') active @endif" href="{{ route('admin.dataadmin') }}">Data Admin</a>
                <a class="collapse-item @if ($pageSlug == 'data_pengguna') active @endif" href="#">Data Pengguna</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link @if ($pageSlug != 'tambah_kategori_baru' or $pageSlug != 'data_kategori') collapsed @endif" href="#" data-toggle="collapse" data-target="#collapseSatu" aria-expanded="true" aria-controls="collapseSatu">
            <i class="fas fa-fw fa-th-list"></i>
            <span>Kategori</span>
        </a>
        <div id="collapseSatu" class="collapse @if ($pageSlug == 'tambah_kategori_baru' or $pageSlug == 'data_kategori') show @endif" aria-labelledby="headingSatu" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item @if ($pageSlug == 'tambah_kategori_baru') active @endif" href="#">Tambah Kategori Baru</a>
                <a class="collapse-item @if ($pageSlug == 'data_kategori') active @endif" href="#">Data Kategori</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link @if ($pageSlug != 'tambah_produk_baru' or $pageSlug != 'data_produk') collapsed @endif" href="#" data-toggle="collapse" data-target="#collapseDua" aria-expanded="true" aria-controls="collapseDua">
            <i class="fas fa-fw fa-th-list"></i>
            <span>Produk</span>
        </a>
        <div id="collapseDua" class="collapse @if ($pageSlug == 'tambah_kategori_baru' or $pageSlug == 'data_kategori') show @endif" aria-labelledby="headingDua" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item @if ($pageSlug == 'tambah_produk_baru') active @endif" href="#">Tambah Produk Baru</a>
                <a class="collapse-item collapse-item @if ($pageSlug == 'data_produk') active @endif" href="#">Data Produk</a>
            </div>
        </div>
    </li>

    <hr class="sidebar-divider d-none d-md-block">

    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>