<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1">

        <link rel="shortcut icon" href="{{ asset('/images/logo-mrt.png') }}" type="image/png">

        <title>@yield('title') | {{ config('app.name') }}</title>

        <link rel="stylesheet" type="text/css" href="{{ asset('/dashboard/vendors/fontawesome-free/css/all.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/dashboard/css/admin.css') }}">
    </head>
    <body id="page-top">

        <div id="wrapper">

            @include('admin.partials.sidebar')

            <div id="content-wrapper" class="d-flex flex-column">

                <div id="content">

                    @include('admin.partials.navbar')

                    <div class="container-fluid">
                        @yield('content')
                    </div>

                </div>

                @include('admin.partials.footer')

            </div>

        </div>

        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <script src="{{ asset('/dashboard/vendors/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('/dashboard/vendors/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('/dashboard/vendors/jquery-easing/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('/dashboard/js/admin.min.js') }}"></script>

    </body>
</html>
