<!DOCTYPE html>
<html lang="en">
<head>
    <title>Masuk | {{ config('app.name') }}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="{{ asset('/images/logo-mrt.png') }}" type="image/png">
    <link rel="stylesheet" type="text/css" href="{{ asset('auth/css/main.css') }}" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<section class="material-half-bg">
    <div class="cover"></div>
</section>
<section class="login-content">
    <div class="login-box">
        <form class="login-form needs-validation" novalidate action="{{ route('admin.login.post') }}" method="POST" role="form">
            @csrf
            <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>Masuk</h3>
            <div class="form-group">
                <label class="control-label" for="email">Alamat Email</label>
                <input class="form-control" type="email" id="email" name="email" placeholder="Alamat Email" autofocus value="{{ old('email') }}" required>
            </div>
            <div class="form-group">
                <label class="control-label" for="password">Kata Sandi</label>
                <input class="form-control" type="password" id="password" name="password" placeholder="Kata Sandi" required>
            </div>
            <div class="form-group">
                <div class="utility">
                    <div class="animated-checkbox">
                        <label>
                            <input type="checkbox" name="remember"><span class="label-text">Tetap Masuk</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group btn-container">
                <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-sign-in fa-lg fa-fw"></i>Masuk</button>
            </div>
        </form>
    </div>
</section>
<script src="{{ asset('auth/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('auth/js/popper.min.js') }}"></script>
<script src="{{ asset('auth/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('auth/js/main.js') }}"></script>
<script src="{{ asset('auth/js/plugins/pace.min.js') }}"></script>

    <script>
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                var forms = document.getElementsByClassName('needs-validation');
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                    form.classList.add('was-validated');
                }, false);
            });
            }, false);
        })();
    </script>
    
</body>
</html>